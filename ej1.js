'use strict';

const data = {};

function handleInputChange({target: {name, value}}) {
    data[name] = value;
}

function addCard() {
    if(data.title && data.description) {
        const container = document.querySelector('.cards-container');
        container.insertAdjacentHTML( 'beforeend', `
        <div class="card">
            <div class="card-header">
                <div class="close-button">
                    <button onclick="deleteCard(event)">X</button>
                </div>
                <div class="card-title">${data.title}</div>
            </div>
            <div class="card-body">${data.description}</div>
        </div>
        `);
        resetData();
    }
}

function deleteCard({target}) {
    target.parentNode.parentNode.parentNode.remove();
}

function resetData() {
    document.querySelectorAll('.input').forEach(input => {
        input.value = "";
    });
    data.title = '';
    data.description = '';
}