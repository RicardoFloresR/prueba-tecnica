'use strict';

let arrayData = [];
let selectedCharacters = [];

window.onload = function() {
    fetch('https://rickandmortyapi.com/api/character')
    .then(response => {
        return response.json()
    })
    .then(data => {
        arrayData = data.results.map(({id, image, name, status}) => {
            return {
                id,
                image,
                name,
                status
            }
        })
        renderOptions();
    })
}

function renderOptions() {
    document.querySelector('.form-select').insertAdjacentHTML( 'beforeend', arrayData.reduce((acc, {id, name}) => {
        acc += `
        <option value="${id}">${name}</option>
        `;
        return acc
    }, ''));
}

function characterSelected({target}) {
    const id = Number(target.value);
    if (id) {
        if(selectedCharacters.includes(id)) {
            window.alert('Elemento ya existente');
        } else {
            selectedCharacters.push(id);
            renderCard(id);
        }
    }
}

function renderCard(id) {
    const {image, name, status} = arrayData.find(item => item.id == id);
    document.querySelector('.cards-container').insertAdjacentHTML( 'beforeend', `
        <div class="card">
            <img src="${image}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <p class="card-text" status="${status}">Status: ${status}</p>
            </div>
        </div>
    `);
}